import React from "react";
import ReactDOM from "react-dom";
import MainPage from "./components/MainPage";
import "./styles/main-styles.css";
import "bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(<MainPage />, document.getElementById("root"));
