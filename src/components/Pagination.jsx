import { Pagination } from "react-bootstrap";
import styled from "styled-components";

const PaginationContainer = styled.div`
  margin-bottom: 50px;
  @media (max-width: 750px) {
    font-size: 15px;
  }
  @media (max-width: 510px) {
    font-size: 10px;
  }
`;

const PaginationComponent = ({ active, setActive }) => {
  const renderItems = () => {
    let items = [];
    for (let number = active - 2; number <= active + 2; number++) {
      if (number > 0) {
        items.push(
          <Pagination.Item
            onClick={() => setActive(number)}
            key={number}
            active={number === active}
          >
            {number}
          </Pagination.Item>
        );
      }
    }
    return items;
  };

  return (
    <PaginationContainer>
      <Pagination>
        {active > 1 && (
          <Pagination.Prev
            onClick={() => setActive((currentPage) => currentPage - 1)}
          />
        )}
        {renderItems()}
        <Pagination.Next
          onClick={() => setActive((currentPage) => currentPage + 1)}
        />
      </Pagination>
    </PaginationContainer>
  );
};

export default PaginationComponent;
