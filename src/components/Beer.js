import React from "react";
import styled from "styled-components";

const BeerContainer = styled.div`
  display: flex;
  justify-content: space-evenly;
  width: 70vw;
  background-color: #e0e2e4;
  margin: 5px;
  padding: 5px;
  border-radius: 5px;
  @media (max-width: 750px) {
    font-size: 15px;
    width: 90vw;
  }
  @media (max-width: 510px) {
    font-size: 10px;
  }
  div,
  a {
    text-align: left;
  }
`;

const Beer = ({ name, website, phone }) => {
  return (
    <>
      <BeerContainer>
        <div class="col-4">{name}</div>
        <a href={website ?? "-"} class="col-5">
          {website ?? "-"}
        </a>
        <div class="col-3">{phone ?? "-"}</div>
      </BeerContainer>
    </>
  );
};
export default Beer;
