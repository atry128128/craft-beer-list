import React from "react";
import Pagination from "./Pagination";
import Spinner from "./Spinner";
import Beer from "./Beer";
import styled from "styled-components";
import fetchdata from "../functions/fetchData";

const WrapperBeers = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 35px;
`;

const WrapperPagination = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 7px;
`;

const BeersList = () => {
  //change this variable if u can show more/less items in page
  const beersPerPage = 15;
  const URL_API = `https://api.openbrewerydb.org/breweries?per_page=${beersPerPage}&page=`;

  const [activePage, setActivePage] = React.useState(1);
  const [beersInPage, setBeersInPage] = React.useState([]);
  const [isLoadingData, setIsLoadingData] = React.useState(false);

  React.useEffect(() => {
    fetchdata(activePage, setBeersInPage, setIsLoadingData, URL_API);
  }, [activePage, URL_API]);

  if (!isLoadingData) {
    return <Spinner />;
  }

  const renderBeers = () => {
    return beersInPage?.map(({ id, name, phone, website_url }) => (
      <Beer key={id} name={name} phone={phone} website={website_url} />
    ));
  };

  return (
    <>
      <WrapperBeers>
        <div>{renderBeers()}</div>
      </WrapperBeers>
      <WrapperPagination>
        <Pagination setActive={setActivePage} active={activePage} />
      </WrapperPagination>
    </>
  );
};

export default BeersList;
