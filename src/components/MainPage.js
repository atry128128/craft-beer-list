import styled from "styled-components";
import BeersList from "../components/BeersList";

const MainPage = () => {
  const MainContainer = styled.div`
    margin: 30px;
    text-align: center;
    @media (max-width: 750px) {
      margin: 15px;
    }
  `;
  return (
    <MainContainer>
      <BeersList />
    </MainContainer>
  );
};

export default MainPage;
