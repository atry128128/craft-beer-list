import React from "react";

import "../styles/spinner-styles.css";

const Spinner = () => {
  return <div className="loader"></div>;
};

export default Spinner;
