const fetchdata = (activePage, setBeersInPage, setIsLoadingData, URL_API) => {
  fetch(URL_API + activePage)
    .then((response) => response.json())
    .then((data) => {
      setBeersInPage(data);
      setIsLoadingData(true);
    });
};
export default fetchdata;
